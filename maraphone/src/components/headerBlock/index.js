import React from 'react'
import s from './headerBlock.module.css'

import logo from '../../logo.png'

const HeaderBlock = () => {
  return (
    <div className={s.cover}>
      <div className={s.wrap}>
        <img src={logo} />
      </div>
    </div>
  )
}

export default HeaderBlock;