import React from 'react'
import s from './maintext.module.sass'

const MainText = () => {
  return (
    <div className={s.text__container}>
      <div className={s.text__opacity}>
        <p className={s.text}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Accusantium aliquid aperiam, architecto aut autem blanditiis
          commodi cum cupiditate deserunt dicta dolore enim eum facere
          fugit id in inventore ipsam itaque libero magnam maiores minima
          mollitia nam nesciunt nisi nobis numquam odio officia perspiciatis
          placeat porro quaerat quas similique sit ullam voluptate voluptatem!
          Aut culpa dolorem ex hic labore laborum omnis? A, cumque delectus,
          distinctio eaque est eveniet inventore ipsa iste nesciunt nulla obcaecati
          porro quod reiciendis reprehenderit rerum. Nostrum, obcaecati, voluptate?
          Commodi dolor enim explicabo odio officiis perspiciatis sint tempora.
          Architecto autem commodi consequatur exercitationem, nemo repudiandae
          velit veniam veritatis!
        </p>
      </div>
    </div>
  )
}

export default MainText;