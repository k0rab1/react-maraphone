import React from 'react'
import s from './footer.module.sass'

const FooterBlock = () => {
  return (
    <div className={s.footer}>
        <h1 className={s.descr}>This is footer</h1>
    </div>
  )
}

export default FooterBlock;