import React from 'react';

import HeaderBlock from './components/headerBlock'
import MainText from './components/mainText'
import FooterBlock from './components/footerBlock'

function App() {
  return (
    <>
      <HeaderBlock />
      <MainText />
      <FooterBlock />
    </>
  );
}

export default App;
